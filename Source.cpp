/*
���������� ������� � ���������� ������ ������ ������� ������
- ��������.

� ������ ��������� �� ���������� ���� ������ ������ ��������
����� ����������� �������.������ ������� ���� ���� ������
������� �� ���� ����������� � �������� � ���������� ����.
������� ������ ������������ ��������� �������� : ����������
������ � ��������� ��� ����������� � add, �������� ������ �
��������� ����������� � remove.��� �������� ��������� �������
������ � ���� ������ � ���������� � ���� ������ �����.
��� ����� ���������� � ��������� ������ �������������� ��
���������.������� ������ ���� � ���� ������ ��������,
���������� �� ������ ��������� ���������� ���������� ��������
�� ������(��� �������� � � �������).

������ � ������������

� �������� ���������� ��� ���� ������ � ����� ������� ������
������������ std::map.
*/

#include<iostream>
#include<map>
#include<exception>

class ZeroStringExeption : public std::exception {
public:
	const char* what() const noexcept override {
		return "Empty vendor code!";
	}
};
class NoSuchProductException : public std::exception {
public:
	const char* what() const noexcept override {
		return "No such product!";
	}
};
class NotEnoughtProductException : public std::exception {
public:
	const char* what() const noexcept override {
		return "Not enought product!";
	}
};


class Basket {
private:
	std::map<std::string, int> basket;
public:
	void add(const std::string vc, const int n,
		std::map <std::string, int>& warehouse) {
		// checking
		if (vc.size() == 0) throw ZeroStringExeption();
		else if (n <= 0) throw std::invalid_argument ("quantity below zero!");
		else if (n > warehouse[vc]) throw NotEnoughtProductException();
		else if (warehouse[vc] == NULL) {
			warehouse.erase(vc);
			throw NoSuchProductException();
		}
		// adding in basket
		basket[vc]+=n;
		// remove from warehouse
		warehouse.at(vc)-=n;
		if (warehouse.at(vc) == 0) warehouse.erase(vc);  // was last item

	}
	void remove(const std::string vc, const int n,
		std::map<std::string,int>& warehouse) {
		//checking
		if (vc.size() == 0) throw ZeroStringExeption();
		else if (n <= 0) throw std::invalid_argument("quantity below zero!");
		else if (n > basket[vc]) throw NotEnoughtProductException();
		else if (basket[vc] == NULL) {
			basket.erase(vc);
			throw NoSuchProductException();
		}
		// return back to warehouse
		warehouse[vc]+=n;
		basket.at(vc)-=n;
		if (basket.at(vc) == 0) basket.erase(vc);  // was last item
	}
	void show() {
		std::cout << "Your basket:\n";
		for (auto it = basket.begin(); it != basket.end(); ++it) {
			std::cout << it->first << " : " << it->second << std::endl;
		}
		std::cout<<std::endl;
	}
};

void showcase(std::map<std::string, int> warehouse) {
	std::cout << "You can buy:\n";
	for (auto it = warehouse.begin(); it != warehouse.end(); ++it) {
		std::cout << it->first << " : " << it->second << std::endl;
	}
	std::cout << std::endl;
}

int main() {
	std::map<std::string, int> warehouse;
	warehouse.insert(std::make_pair("balensiaga", 3));
	warehouse.insert(std::make_pair("guchi", 2));
	warehouse.insert(std::make_pair("armyane", 1));

	std::unique_ptr<Basket> basket(new Basket());
	bool exit = false;

	try {
		while (!exit) {
			showcase(warehouse);
			std::cout << "Please type \"add\" for adding product in basket\n";
			std::cout << "& \"remove\" for remove product from basket\n";
			std::string temp;
			std::cin >> temp;
			if (temp == "add") {
				std::cout << "Please input vendor code & quantity\n";
				std::string vc;
				int q = 0;
				std::cin >> vc >> q;
				basket->add(vc, q, warehouse);
				basket->show();
			}
			else if (temp == "remove") {
				std::cout << "Please input vendor code & quantity\n";
				std::string vc;
				int q = 0;
				std::cin >> vc >> q;
				basket->remove(vc, q, warehouse);
				basket->show();
			}
			else if (temp == "exit") exit = true;
		}
	}	
	catch (const ZeroStringExeption& x) {
		std::cerr << x.what() << std::endl;
		exit = true;
	}
	catch (const NoSuchProductException& x) {
		std::cerr << x.what() << std::endl;
		exit = true;
	}
	catch (const NotEnoughtProductException& x) {
		std::cerr << x.what() << std::endl;
		exit = true;
	}
	catch (...) {
		std::cerr << "Something went wrong\n" << std::endl;
		exit = true;
	}
}